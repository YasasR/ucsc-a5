int profit(int price);
int revenue(int price);
int attendance(int price);
int expenditure(int price);

int attendance(int price){
    const int att=120;
    return att-((price-15) /5*20);
}

int revenue(int price){
    return price*attendance(price);
}

int expenditure(int price){
    const int x=500;
    return x+3*attendance(price);
}

int profit(int price){
   return revenue(price)-expenditure(price);
}

int main(){
	printf("Price \t Profit\n");
	int i=5,profitm;
	for (i=5;i<=50;i+=5){
		 profitm= profit(i);
		 printf("Rs.%d \t Rs.%d\n",i,profitm);
	}
	printf("Maximum profit is Rs.1260 \nTicket price Rs.25");

    return 0;
}
